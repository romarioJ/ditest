package com.GuiceTest;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MsgHandler_MembersInjector implements MembersInjector<MsgHandler> {
  private final Provider<InboxM> inboxMProvider;

  private final Provider<OutBoxM> outBoxMProvider;

  public MsgHandler_MembersInjector(
      Provider<InboxM> inboxMProvider, Provider<OutBoxM> outBoxMProvider) {
    assert inboxMProvider != null;
    this.inboxMProvider = inboxMProvider;
    assert outBoxMProvider != null;
    this.outBoxMProvider = outBoxMProvider;
  }

  public static MembersInjector<MsgHandler> create(
      Provider<InboxM> inboxMProvider, Provider<OutBoxM> outBoxMProvider) {
    return new MsgHandler_MembersInjector(inboxMProvider, outBoxMProvider);
  }

  @Override
  public void injectMembers(MsgHandler instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.inboxM = inboxMProvider.get();
    instance.outBoxM = outBoxMProvider.get();
  }

  public static void injectInboxM(MsgHandler instance, Provider<InboxM> inboxMProvider) {
    instance.inboxM = inboxMProvider.get();
  }

  public static void injectOutBoxM(MsgHandler instance, Provider<OutBoxM> outBoxMProvider) {
    instance.outBoxM = outBoxMProvider.get();
  }
}
