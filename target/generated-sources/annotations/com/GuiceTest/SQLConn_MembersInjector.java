package com.GuiceTest;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class SQLConn_MembersInjector implements MembersInjector<SQLConn> {
  private final Provider<String> connectionHostAndUserNameProvider;

  public SQLConn_MembersInjector(Provider<String> connectionHostAndUserNameProvider) {
    assert connectionHostAndUserNameProvider != null;
    this.connectionHostAndUserNameProvider = connectionHostAndUserNameProvider;
  }

  public static MembersInjector<SQLConn> create(
      Provider<String> connectionHostAndUserNameProvider) {
    return new SQLConn_MembersInjector(connectionHostAndUserNameProvider);
  }

  @Override
  public void injectMembers(SQLConn instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.userName = connectionHostAndUserNameProvider.get();
    instance.connectionHost = connectionHostAndUserNameProvider.get();
  }

  public static void injectUserName(SQLConn instance, Provider<String> userNameProvider) {
    instance.userName = userNameProvider.get();
  }

  public static void injectConnectionHost(
      SQLConn instance, Provider<String> connectionHostProvider) {
    instance.connectionHost = connectionHostProvider.get();
  }
}
