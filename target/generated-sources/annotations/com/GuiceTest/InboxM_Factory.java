package com.GuiceTest;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class InboxM_Factory implements Factory<InboxM> {
  private final Provider<IStore> iStoreProvider;

  public InboxM_Factory(Provider<IStore> iStoreProvider) {
    assert iStoreProvider != null;
    this.iStoreProvider = iStoreProvider;
  }

  @Override
  public InboxM get() {
    return new InboxM(iStoreProvider.get());
  }

  public static Factory<InboxM> create(Provider<IStore> iStoreProvider) {
    return new InboxM_Factory(iStoreProvider);
  }
}
