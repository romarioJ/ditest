package com.GuiceTest;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class OutBoxM_MembersInjector implements MembersInjector<OutBoxM> {
  private final Provider<IStore> iStoreProvider;

  public OutBoxM_MembersInjector(Provider<IStore> iStoreProvider) {
    assert iStoreProvider != null;
    this.iStoreProvider = iStoreProvider;
  }

  public static MembersInjector<OutBoxM> create(Provider<IStore> iStoreProvider) {
    return new OutBoxM_MembersInjector(iStoreProvider);
  }

  @Override
  public void injectMembers(OutBoxM instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.iStore = iStoreProvider.get();
  }

  public static void injectIStore(OutBoxM instance, Provider<IStore> iStoreProvider) {
    instance.iStore = iStoreProvider.get();
  }
}
