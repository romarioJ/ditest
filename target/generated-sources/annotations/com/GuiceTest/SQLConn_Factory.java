package com.GuiceTest;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class SQLConn_Factory implements Factory<SQLConn> {
  private final Provider<String> connectionHostAndPasswordAndUserNameProvider;

  public SQLConn_Factory(Provider<String> connectionHostAndPasswordAndUserNameProvider) {
    assert connectionHostAndPasswordAndUserNameProvider != null;
    this.connectionHostAndPasswordAndUserNameProvider =
        connectionHostAndPasswordAndUserNameProvider;
  }

  @Override
  public SQLConn get() {
    return new SQLConn(
        connectionHostAndPasswordAndUserNameProvider.get(),
        connectionHostAndPasswordAndUserNameProvider.get(),
        connectionHostAndPasswordAndUserNameProvider.get());
  }

  public static Factory<SQLConn> create(
      Provider<String> connectionHostAndPasswordAndUserNameProvider) {
    return new SQLConn_Factory(connectionHostAndPasswordAndUserNameProvider);
  }
}
