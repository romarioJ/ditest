package com.DataStore;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class TestModule_ProvideuserNameFactory implements Factory<String> {
  private final TestModule module;

  public TestModule_ProvideuserNameFactory(TestModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public String get() {
    return Preconditions.checkNotNull(
        module.provideuserName(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<String> create(TestModule module) {
    return new TestModule_ProvideuserNameFactory(module);
  }
}
