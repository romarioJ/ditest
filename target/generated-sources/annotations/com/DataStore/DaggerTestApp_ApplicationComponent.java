package com.DataStore;

import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerTestApp_ApplicationComponent implements TestApp.ApplicationComponent {
  private Provider<String> provideuserNameProvider;

  private Provider<String> providepasswordProvider;

  private Provider<String> provideconnectionHoseProvider;

  private Provider<SQLConn> sQLConnProvider;

  private Provider<String> providemonogDBNameProvider;

  private Provider<String> providemongoDbServerAddressProvider;

  private Provider<mongoDBConnect> mongoDBConnectProvider;

  private DaggerTestApp_ApplicationComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static TestApp.ApplicationComponent create() {
    return builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.provideuserNameProvider = TestModule_ProvideuserNameFactory.create(builder.testModule);

    this.providepasswordProvider = TestModule_ProvidepasswordFactory.create(builder.testModule);

    this.provideconnectionHoseProvider =
        TestModule_ProvideconnectionHoseFactory.create(builder.testModule);

    this.sQLConnProvider =
        SQLConn_Factory.create(
            provideuserNameProvider, providepasswordProvider, provideconnectionHoseProvider);

    this.providemonogDBNameProvider =
        TestModule_ProvidemonogDBNameFactory.create(builder.testModule);

    this.providemongoDbServerAddressProvider =
        TestModule_ProvidemongoDbServerAddressFactory.create(builder.testModule);

    this.mongoDBConnectProvider =
        mongoDBConnect_Factory.create(
            providemonogDBNameProvider, providemongoDbServerAddressProvider);
  }

  @Override
  public SQLConn getSQLConn() {
    return sQLConnProvider.get();
  }

  @Override
  public mongoDBConnect getMongoConn() {
    return mongoDBConnectProvider.get();
  }

  public static final class Builder {
    private TestModule testModule;

    private Builder() {}

    public TestApp.ApplicationComponent build() {
      if (testModule == null) {
        this.testModule = new TestModule();
      }
      return new DaggerTestApp_ApplicationComponent(this);
    }

    public Builder testModule(TestModule testModule) {
      this.testModule = Preconditions.checkNotNull(testModule);
      return this;
    }
  }
}
