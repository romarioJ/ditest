package com.DataStore;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class SQLConn_Factory implements Factory<SQLConn> {
  private final Provider<String> userNameProvider;

  private final Provider<String> passwordProvider;

  private final Provider<String> connectionHostProvider;

  public SQLConn_Factory(
      Provider<String> userNameProvider,
      Provider<String> passwordProvider,
      Provider<String> connectionHostProvider) {
    assert userNameProvider != null;
    this.userNameProvider = userNameProvider;
    assert passwordProvider != null;
    this.passwordProvider = passwordProvider;
    assert connectionHostProvider != null;
    this.connectionHostProvider = connectionHostProvider;
  }

  @Override
  public SQLConn get() {
    return new SQLConn(
        userNameProvider.get(), passwordProvider.get(), connectionHostProvider.get());
  }

  public static Factory<SQLConn> create(
      Provider<String> userNameProvider,
      Provider<String> passwordProvider,
      Provider<String> connectionHostProvider) {
    return new SQLConn_Factory(userNameProvider, passwordProvider, connectionHostProvider);
  }
}
