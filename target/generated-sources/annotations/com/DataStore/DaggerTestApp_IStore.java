package com.DataStore;

import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerTestApp_IStore implements TestApp.IStore {
  private Provider<String> provideuserNameProvider;

  private Provider<String> providepasswordProvider;

  private Provider<String> provideconnectionHoseProvider;

  private Provider<SQLConn> sQLConnProvider;

  private DaggerTestApp_IStore(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static TestApp.IStore create() {
    return builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.provideuserNameProvider = TestModule_ProvideuserNameFactory.create(builder.testModule);

    this.providepasswordProvider = TestModule_ProvidepasswordFactory.create(builder.testModule);

    this.provideconnectionHoseProvider =
        TestModule_ProvideconnectionHoseFactory.create(builder.testModule);

    this.sQLConnProvider =
        SQLConn_Factory.create(
            provideuserNameProvider, providepasswordProvider, provideconnectionHoseProvider);
  }

  @Override
  public SQLConn sQLConn() {
    return sQLConnProvider.get();
  }

  public static final class Builder {
    private TestModule testModule;

    private Builder() {}

    public TestApp.IStore build() {
      if (testModule == null) {
        this.testModule = new TestModule();
      }
      return new DaggerTestApp_IStore(this);
    }

    public Builder testModule(TestModule testModule) {
      this.testModule = Preconditions.checkNotNull(testModule);
      return this;
    }
  }
}
