package com.DataStore;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class mySQLConn_Factory implements Factory<mySQLConn> {
  private final Provider<String> connectionHostAndPasswordAndUserNameProvider;

  public mySQLConn_Factory(Provider<String> connectionHostAndPasswordAndUserNameProvider) {
    assert connectionHostAndPasswordAndUserNameProvider != null;
    this.connectionHostAndPasswordAndUserNameProvider =
        connectionHostAndPasswordAndUserNameProvider;
  }

  @Override
  public mySQLConn get() {
    return new mySQLConn(
        connectionHostAndPasswordAndUserNameProvider.get(),
        connectionHostAndPasswordAndUserNameProvider.get(),
        connectionHostAndPasswordAndUserNameProvider.get());
  }

  public static Factory<mySQLConn> create(
      Provider<String> connectionHostAndPasswordAndUserNameProvider) {
    return new mySQLConn_Factory(connectionHostAndPasswordAndUserNameProvider);
  }
}
