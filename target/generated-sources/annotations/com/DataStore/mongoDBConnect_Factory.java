package com.DataStore;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class mongoDBConnect_Factory implements Factory<mongoDBConnect> {
  private final Provider<String> databasenameAndServeraddressProvider;

  public mongoDBConnect_Factory(Provider<String> databasenameAndServeraddressProvider) {
    assert databasenameAndServeraddressProvider != null;
    this.databasenameAndServeraddressProvider = databasenameAndServeraddressProvider;
  }

  @Override
  public mongoDBConnect get() {
    return new mongoDBConnect(
        databasenameAndServeraddressProvider.get(), databasenameAndServeraddressProvider.get());
  }

  public static Factory<mongoDBConnect> create(
      Provider<String> databasenameAndServeraddressProvider) {
    return new mongoDBConnect_Factory(databasenameAndServeraddressProvider);
  }
}
