package com.DataStore;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class TestModule_ProvideSQLconnectionHoseFactory implements Factory<String> {
  private final TestModule module;

  public TestModule_ProvideSQLconnectionHoseFactory(TestModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public String get() {
    return Preconditions.checkNotNull(
        module.provideSQLconnectionHose(),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<String> create(TestModule module) {
    return new TestModule_ProvideSQLconnectionHoseFactory(module);
  }
}
