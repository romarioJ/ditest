package com.GuiceTest;

import javax.inject.Inject;

public class mongoDBConnect implements IStore {

        private String databasename;
        private String serveraddress;


        @Inject
    public mongoDBConnect(String databasename, String serveraddress) {
        this.databasename = databasename;
        this.serveraddress = serveraddress;
        }

    public String getDatabasename() {
        return databasename;
    }

    public void setDatabasename(String databasename) {
        this.databasename = databasename;
    }

    public String getServeraddress() {
        return serveraddress;
    }

    public void setServeraddress(String serveraddress) {
        this.serveraddress = serveraddress;
    }


    public void connect() {
        System.out.println("Connected to mongoDB" + getDatabasename() + " server address " + getServeraddress() );
    }


}
