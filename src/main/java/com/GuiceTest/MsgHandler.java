package com.GuiceTest;

import javax.inject.Inject;
import javax.inject.Named;

public class MsgHandler {

    //Here message Handler uses instance of inbox and out box
    //Therefore those two objects need to be injected as well

    @Inject
            @Named("inboxM")
    InboxM inboxM;

    @Inject
            @Named("outboxM")
    OutBoxM outBoxM;

    public MsgHandler(){}

    public void startHandlingMessages(){

        outBoxM.connectToDB();

        inboxM.connectToDB();

    }

}
