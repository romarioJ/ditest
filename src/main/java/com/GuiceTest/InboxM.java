package com.GuiceTest;

import javax.inject.Inject;
import javax.inject.Named;

public class InboxM {

    //In this class a Istore type instance is needed of SQL Conn. Therefore Istore type is a dependency here. There fore its is injected


    IStore iStore;

    @Inject
    public InboxM( @Named("test") IStore iStore) {

        this.iStore = iStore;
        System.out.println("Im here");
    }

    public void connectToDB(){
        iStore.connect();
    }
}
