package com.GuiceTest;

import com.google.inject.name.Named;

import javax.inject.Inject;


public class SQLConn implements IStore {

    //Also can inject to the fields instead of constructor
    //Need a No arg constructor in that case
    String userName;
    String password;
    String connectionHost;


    //Inject to this constructor
    //Put @Inject annotation and for each parameter use @Named so u can identify it and then give the parameter u want

    @Inject public SQLConn(@Named("SQLuserName") String userName, @Named("SQLpassword") String password, @Named("SQLconnectionHost") String connectionHost) throws Exception{
        this.userName = userName;
        this.password = password;
        this.connectionHost = connectionHost;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConnectionHost() {
        return connectionHost;
    }

    public void setConnectionHost(String connectionHost) {
        this.connectionHost = connectionHost;
    }

    public void connect(){
        System.out.println("Connected to : " + getConnectionHost() + " with the following credentials" + getPassword() + " " +  getUserName());
    }

}
