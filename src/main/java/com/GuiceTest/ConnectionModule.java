package com.GuiceTest;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class ConnectionModule extends AbstractModule {

    @Override
    protected void configure() {

        //Here we bind the String fields in the SQL conn class
        bindConstant().annotatedWith(Names.named("SQLuserName")).to(" TEST USER NAME ");
        bindConstant().annotatedWith(Names.named("SQLpassword")).to(" TEST PASSWORD ");
        bindConstant().annotatedWith(Names.named("SQLconnectionHost")).to(" TEST HOST ");

        //Here we can just bing the SQLCon class to any instance of IStore class that is annotated with "test" to SQLConn  straight away since the dependencies for SQLConn have been injected aboce
        //Exceptions are not a problem in this way
        bind(IStore.class).annotatedWith(Names.named("test")).to(SQLConn.class);


        try {
            //If not if you want multiple instances with the SQLCon class having different connection details this method can be used as well
            //If a exception is thrown in the constructor this needs to be in a try catch
            bind(IStore.class).annotatedWith(Names.named("test2")).toInstance(new SQLConn("test2","test2","test2"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Here a instance of Inbox is made to be used where ever it has to be injected
        bind(InboxM.class).annotatedWith(Names.named("inboxM")).to(InboxM.class);

        //Create an instance of outbox whereever it needs to be injected
        bind(OutBoxM.class).annotatedWith(Names.named("outboxM")).to(OutBoxM.class);

    }


}
