package com.GuiceTest;

import javax.inject.Inject;
import javax.inject.Named;

public class OutBoxM {

    //In this class a Istore type instance is needed of SQL Conn. Therefore Istore type is a dependency here. There fore its is injected to a field
    //a no arg constructor is needed in this case

    @Inject
    @Named("test2")
    IStore iStore;

    public OutBoxM(){

    }

    public OutBoxM(IStore iStore) {
        this.iStore = iStore;
    }

    public void connectToDB(){
        iStore.connect();
    }

}
