package com.GuiceTest;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class TestApp {

    public static void main(String[] args) {

        Injector sqlInjector = Guice.createInjector(new ConnectionModule());

      /*  InboxM inboxM = sqlInjector.getInstance(InboxM.class);
        inboxM.connectToDB();

        OutBoxM outBoxM = sqlInjector.getInstance(OutBoxM.class);
        outBoxM.connectToDB();*/

      MsgHandler msgHandler = sqlInjector.getInstance(MsgHandler.class);
      msgHandler.startHandlingMessages();


     /*  TestDI testDI = new TestDI(new SQLConn("UN", "pw","TEST H"));
       testDI.connectToDB();*/

      /*  SQLConn sqlConn1 = new SQLConn("UN", "pw","TEST H");
        sqlConn1.connect();*/

    }
}
